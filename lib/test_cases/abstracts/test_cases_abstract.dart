import 'package:general_libraries_examples_dart/general_failure/libraries/general_failure_library.dart';
import 'package:meta/meta.dart';
import 'package:test/test.dart';

@immutable
abstract class TestCaseStructureAbstract {
  final bool positive;
  final String description;
  final dynamic matcher;

  TestCaseStructureAbstract({@required this.positive, @required this.description, @required this.matcher});
}

@immutable
abstract class TestCasesAbstract {
  @protected
  final String testGroup = '[Class]';

  String get getTestGroup => testGroup;
  List<TestCaseStructureAbstract> get getTestCases => definedTestCases();

  @protected
  List<TestCaseStructureAbstract> definedTestCases();
  // {
  //   // TODO: Can this be made immutable
  //   var testCases = <TestCaseStructure>Abstract[];

  //   testCases.add(TestCaseStructureAbstract(
  //     positive: true,
  //     description: '0',
  //     matcher: 0,
  //   ));

  //   return testCases;
  // }
}

@immutable
abstract class ClassTestsAbstract {
  @protected
  final String testGroup;
  @protected
  final List<TestCaseStructureAbstract> testCases;

  ClassTestsAbstract({@required this.testGroup, @required this.testCases}) {
    testCases.isNotEmpty ? testTestCases() : GeneralFailure(generalFailure: 'No Test Cases');
  }

  @protected
  void testTestCases() {
    group('Class: $testGroup', () {
      setUp(() {});

      testCases.forEach((testCase) => testTestCase(testCase: testCase));
    });
  }

  @protected
  void testTestCase({@required TestCaseStructureAbstract testCase});
  // {
  //   final testCaseDowncast = testCase as TestCaseStructureAbstract;

  //   final testType = testCaseDowncast.positive ? 'Positive:' : 'Negative:';

  //   test('$testType ${testCaseDowncast.description}', () async {
  //     // Arrange
  //     final testClass = TestClass([testClassArguments]);

  //     // Act
  //     final actual = testClass.testMethod([testMethodArguments]);

  //     // Assert
  //     expect(actual, testCaseDowncast.matcher);
  //   });
  // }
}

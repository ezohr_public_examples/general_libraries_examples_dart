library string_general_library;

import 'package:meta/meta.dart';

@immutable
class StringGeneral {
  static String sentenceCase({@required String string}) {
    return string[0].toUpperCase() + string.substring(1).toLowerCase();
  }
}

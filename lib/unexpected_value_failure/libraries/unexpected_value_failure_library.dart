library unexpected_value_failure_library.dart;

// TODO: UnexpectedValueFailureLibrary Unit Tests using the Test Cases Framework for Left and Right use cases

import 'package:general_libraries_examples_dart/value_object_and_common/abstracts/value_object_and_common_abstract.dart';
import 'package:meta/meta.dart';

@immutable
class UnexpectedValueFailureLibrary extends Error {
  @protected
  final ValueFailuresAbstract valueFailure;

  UnexpectedValueFailureLibrary({this.valueFailure});

  @override
  @protected
  String toString() {
    return Error.safeToString('ERROR: Unexpected Value Failure: $valueFailure');
  }
}

library value_object_and_common_library;

import 'package:dartz/dartz.dart';
import 'package:general_libraries_examples_dart/unexpected_value_failure/libraries/unexpected_value_failure_library.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ValueObjectAbstract<T> {
  @protected
  final Either<ValueFailuresAbstract<T>, T> value;

  // factory ValueObjectAbstract({@required T value}) {
  //   final validatedValue = ValueValidationAbstract.validate(value: value);

  //   return ValueObjectAbstract.private(value: validatedValue);
  // }

  @protected
  ValueObjectAbstract.private({@required this.value});

  bool get isValid => value.isRight();
  Either<ValueFailuresAbstract<T>, T> get getValue => value;

  // TODO: UnexpectedValueFailureLibrary Unit Tests using the Test Cases Framework for Left and Right use cases
  // id becomes the equivalent of (right) => right);
  T unexpectedValueFailure() {
    return value.fold((l) => throw UnexpectedValueFailureLibrary(valueFailure: l), id);
  }

  // Value Equality (Generated using Dart Data Class Generator extension)

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ValueObjectAbstract<T> && o.value == value;
  }

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'Value($value)';
}

@immutable
abstract class ValueValidationAbstract<T> {}

@immutable
abstract class ValueFailuresAbstract<T> {}

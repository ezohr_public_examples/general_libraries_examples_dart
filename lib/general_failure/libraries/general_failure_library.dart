library general_failure_library.dart;

// TODO: GeneralFailure Unit Tests using the Test Cases Framework
// TODO: Alternate options to just printing to the screen

import 'package:meta/meta.dart';

@immutable
// class GeneralFailure extends Error {
class GeneralFailure {
  @protected
  final String generalFailure;

  GeneralFailure({this.generalFailure}) {
    print(toString());
  }

  @override
  @protected
  String toString() {
    return Error.safeToString('ERROR: General Failure: $generalFailure');
  }
}

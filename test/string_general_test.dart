import 'package:general_libraries_examples_dart/string_general/libraries/string_general.dart';
import 'package:general_libraries_examples_dart/test_cases/abstracts/test_cases_abstract.dart';
import 'package:meta/meta.dart';
import 'package:test/test.dart';

@immutable
class TestCaseStructure extends TestCaseStructureAbstract {
  final String string;

  TestCaseStructure({@required positive, @required description, @required matcher, @required this.string}) : super(positive: positive, description: description, matcher: matcher);
}

@immutable
class TestCases extends TestCasesAbstract {
  @protected
  @override
  final String testGroup = 'StringGeneral';

  @override
  @protected
  List<TestCaseStructure> definedTestCases() {
    // TODO: Can this be made immutable
    var testCases = <TestCaseStructure>[];

    testCases.add(TestCaseStructure(
      positive: true,
      description: 'hello, number 1!',
      matcher: 'Hello, number 1!',
      string: 'hello, number 1!',
    ));

    testCases.add(TestCaseStructure(
      positive: false,
      description: 'hElLo, NuMbEr 1!',
      matcher: 'Hello, number 1!',
      string: 'hElLo, NuMbEr 1!',
    ));

    return testCases;
  }
}

@immutable
class ClassTests extends ClassTestsAbstract {
  ClassTests({@required String testGroup, @required List<TestCaseStructure> testCases}) : super(testGroup: testGroup, testCases: testCases);

  @override
  @protected
  void testTestCase({@required TestCaseStructureAbstract testCase}) {
    final testCaseDowncast = testCase as TestCaseStructure;

    final testType = testCaseDowncast.positive ? 'Positive:' : 'Negative:';

    test('$testType ${testCaseDowncast.description}', () async {
      // Arrange

      // Act
      final actual = StringGeneral.sentenceCase(string: testCaseDowncast.string);

      // Assert
      expect(actual, testCaseDowncast.matcher);
    });
  }
}

void main() {
  final testCases = TestCases();

  ClassTests(testGroup: testCases.getTestGroup, testCases: testCases.getTestCases);
}

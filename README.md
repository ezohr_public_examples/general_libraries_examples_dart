# General Libraries Examples (Dart)

## String General

> Sentence Case converter
>
> Sentence Case Unit Tests using the Test Cases Framework

## Test Cases

> Test Cases Framework to make Unit Tests simple and manageable

## Value Object And Common

> Value Object and Common Getters Framework using the Dartz Package
